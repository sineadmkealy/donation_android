package app.donation.model;

/**
 * Model key application domain - candidates for objects to be stored in a database:
 * locally (sql_lite); remove (via API)
 */
public class Donation
{
    public String _id;
    public int    amount;
    public String method;

    public Donation (int amount, String method)
    {
        this.amount = amount;
        this.method = method;
    }
}