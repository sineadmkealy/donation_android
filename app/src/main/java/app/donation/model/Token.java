package app.donation.model;

/**
 * model class to hold the Json Web Tokens in the app
 */
public class Token
{
  public boolean success;
  public String token;
  public User user;

  public Token(boolean success, String token)
  {
    this.success = success;
    this.token = token;
  }
}