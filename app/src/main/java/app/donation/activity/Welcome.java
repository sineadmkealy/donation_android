package app.donation.activity;

import app.donation.R;
import app.donation.main.DonationApp;
import app.donation.model.Candidate;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Toast;

import java.util.List;

public class Welcome extends AppCompatActivity
{
  private DonationApp app;

  @Override
  public void onCreate(Bundle savedInstanceState)
  {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_welcome);

    app = (DonationApp) getApplication();
  }

  /**
   * returns Users
   * triggers onResponse or failure method
   * could use anonymous class here either - makes more sense, Java can only handle so many callbacks
   */
  @Override
  public void onResume()
  {
    super.onResume();
    app.currentUser = null;;
    // requesting a list of candidates from the remote service, and storing them in the app object
     // uses an anonymous inner class instead of the Welcome class as the implementor of the callback interface
    Call<List<Candidate>> call = (Call<List<Candidate>>) app.donationServiceOpen.getAllCandidates();
    call.enqueue(new Callback<List<Candidate>>() { // this is type of interface (new).. Lambda will improve this convoluted verbose syntax
      @Override
      public void onResponse(Call<List<Candidate>> call, Response<List<Candidate>> response) {
        serviceAvailableMessage();
        app.candidates = response.body();
      }

      /**
       * onFailure sets a flag in app, and displays an error message
       */
      @Override
      public void onFailure(Call<List<Candidate>> call, Throwable t) {
        serviceUnavailableMessage();
      }
    });
  }

  public void loginPressed (View view)
  {
    if (app.donationServiceAvailable)
    {
      startActivity (new Intent(this, Login.class));
    }
    else
    {
      serviceUnavailableMessage();
    }
  }

  public void signupPressed (View view)
  {
    if (app.donationServiceAvailable)
    {
      startActivity (new Intent(this, Signup.class));
    }
    else
    {
      serviceUnavailableMessage();
    }
  }

  void serviceUnavailableMessage()
  {
    app.donationServiceAvailable = false;
    Toast toast = Toast.makeText(this, "Donation Service Unavailable. Try again later", Toast.LENGTH_LONG);
    toast.show();
  }

  void serviceAvailableMessage()
  {
    app.donationServiceAvailable = true;
    Toast toast = Toast.makeText(this, "Donation Contacted Successfully", Toast.LENGTH_LONG);
    toast.show();
  }
}
